package dto;

public class DeliveryAddressInformation {

    private final String fullName;
    private final String deliveryCountry;
    private final String addressLine1;
    private final String addressLine2;
    private final String townCity;
    private final String countyState;
    private final String postcodeZIP;


    public DeliveryAddressInformation(String fullName, String deliveryCountry, String addressLine1,
                                      String addressLine2, String townCity, String countyState, String postcodeZIP) {
        this.fullName = fullName;
        this.deliveryCountry = deliveryCountry;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.townCity = townCity;
        this.countyState = countyState;
        this.postcodeZIP = postcodeZIP;
    }

    public String getFullName() {
        return fullName;
    }

    public String getDeliveryCountry() {
        return deliveryCountry;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public String getTownCity() {
        return townCity;
    }

    public String getCountyState() {
        return countyState;
    }

    public String getPostcodeZIP() {
        return postcodeZIP;
    }
}
