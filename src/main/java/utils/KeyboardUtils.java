package utils;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import static driver.SingletonDriver.getDriver;

public class KeyboardUtils {

    public static void submit() {
        Actions action = new Actions(getDriver());
        action.sendKeys(Keys.ENTER).perform();
    }

    public static void moveOutOfTheField(WebElement field) {
        field.sendKeys(Keys.TAB);
    }
}
