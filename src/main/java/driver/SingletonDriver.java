package driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static driver.CapabilitiesHelper.getChromeOptions;

public class SingletonDriver {

    private static WebDriver instance;

    private SingletonDriver() {
    }

    public static WebDriver getDriver() {
        return instance;
    }

    public static void openPageByUrl(String url) {
        instance.get(url);
    }

    public static void createDriver() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        instance = new ChromeDriver(getChromeOptions());
    }
}
