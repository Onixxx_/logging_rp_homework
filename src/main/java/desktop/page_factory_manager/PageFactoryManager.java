package desktop.page_factory_manager;

import abstractClasses.page.AbstractPage;
import desktop.pages.BasketPage;
import desktop.pages.CheckoutPage;
import desktop.pages.InitialHomePage;
import desktop.pages.SearchPage;

public class PageFactoryManager {

    public AbstractPage getPageByName(String pageName) {
        PageNames page = PageNames.valueOf(pageName.replace(" ", "_").toUpperCase());
        switch (page) {
            case INITIAL_HOME:
                return getHomePage();
            case SEARCH:
                return getSearchPage();
            case BASKET:
                return getBasketPage();
            case CHECKOUT:
                return getCheckoutPage();
            default:
                return null;
        }
    }

    public InitialHomePage getHomePage() {
        return new InitialHomePage();
    }

    public SearchPage getSearchPage() {
        return new SearchPage();
    }

    public BasketPage getBasketPage() {
        return new BasketPage();
    }

    public CheckoutPage getCheckoutPage() {
        return new CheckoutPage();
    }
}
