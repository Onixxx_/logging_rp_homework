package desktop.page_factory_manager;

public enum PageNames {
    INITIAL_HOME,
    SEARCH,
    BASKET,
    CHECKOUT
}
