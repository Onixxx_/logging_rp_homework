package desktop.fragments.order_summary;

import abstractClasses.fragment.AbstractFragment;

import java.util.Map;

public abstract class AbstractOrderSummary extends AbstractFragment {

    public abstract Map<String, String> getTotals();

    public boolean isOrderSummaryAsFollowing(Map<String, String> expectedTotals) {
        Map<String, String> actualTotals = getTotals();
        return expectedTotals.entrySet().stream()
                .allMatch(element -> element.getValue().equals(actualTotals.get(element.getKey())));
    }
}
