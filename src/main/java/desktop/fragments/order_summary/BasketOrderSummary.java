package desktop.fragments.order_summary;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.HashMap;
import java.util.Map;

public class BasketOrderSummary extends AbstractOrderSummary {

    @FindBy(css = "div.basket-totals")
    private WebElement rootElement;

    public BasketOrderSummary() {
        setRootElement(rootElement);
    }
    @Override
    public Map<String, String> getTotals() {
        Map<String, String> totals = new HashMap<>();
        totals.put("Delivery cost", getChildElement(getDeliveryCost()).getText());
        totals.put("Total", getChildElement(getTotal()).getText());
        return totals;
    }

    private By getDeliveryCost() {
        return By.cssSelector("dl.delivery-text dd");
    }

    private By getTotal() {
        return By.cssSelector("dl.total dd");
    }
}
