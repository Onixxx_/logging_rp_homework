package desktop.fragments.order_summary;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.HashMap;
import java.util.Map;

public class CheckoutOrderSummary extends AbstractOrderSummary {

    @FindBy(css = "div.mini-basket")
    private WebElement rootElement;

    public CheckoutOrderSummary() {
        setRootElement(rootElement);
    }

    @Override
    public Map<String, String> getTotals() {
        Map<String, String> totals = new HashMap<>();
        totals.put("Sub-total", getChildElement(getSubTotal()).getText());
        totals.put("Delivery", getChildElement(getDelivery()).getText());
        totals.put("VAT", getChildElement(getVAT()).getText());
        totals.put("Total", getChildElement(getTotal()).getText());
        return totals;
    }

    private By getSubTotal() {
        return By.cssSelector("div.wrapper dl:nth-child(2) dd");
    }

    private By getDelivery() {
        return By.cssSelector("div.wrapper dl:nth-child(3) dd");
    }

    private By getVAT() {
        return By.cssSelector("div.wrapper dl:nth-child(4) dd");
    }

    private By getTotal() {
        return By.cssSelector("div.wrapper dl:nth-child(5) dd");
    }
}
