package desktop.fragments.forms;

import abstractClasses.fragment.AbstractFragment;
import dto.DeliveryAddressInformation;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.HasFormFields;

import java.util.HashMap;
import java.util.Map;

import static constants.DeliveryAddressFormConstants.*;
import static utils.JavaScriptExecutorActions.scrollToElement;
import static utils.KeyboardUtils.moveOutOfTheField;

public class DeliveryAddressForm extends AbstractFragment implements HasFormFields {

    @FindBy(css = "div.delivery-address")
    private WebElement rootElement;

    public DeliveryAddressForm() {
        setRootElement(rootElement);
    }

    @Override
    public Map<String, FormField> getFormFields() {
        Map<String, FormField> formFields = new HashMap<>();
        formFields.put(EMAIL_ADDRESS, new FormField(getChildElement(getEmailInput()),
                getChildElement(getEmailInputError())));
        formFields.put(FULL_NAME, new FormField(getChildElement(getFullNameInput()),
                getChildElement(getFullNameInputError())));
        formFields.put(DELIVERY_COUNTRY, new FormField(getChildElement(getCountryInput())));
        formFields.put(ADDRESS_LINE_1, new FormField(getChildElement(getAddressLine1Input()),
                getChildElement(getAddressLine1InputError())));
        formFields.put(ADDRESS_LINE_2, new FormField(getChildElement(getAddressLine2Input())));
        formFields.put(CITY, new FormField(getChildElement(getCityInput()),
                getChildElement(getCityInputError())));
        formFields.put(COUNTY, new FormField(getChildElement(getCountyInput())));
        formFields.put(POSTCODE, new FormField(getChildElement(getPostcodeInput()),
                getChildElement(getPostcodeInputError())));

        return formFields;
    }

    public void provideEmail(String value) {
        scrollToElement(getChildElement(getEmailInput()));
        setInputValue(EMAIL_ADDRESS, value);
    }

    public void fillDeliveryAddressForm(DeliveryAddressInformation information) {
        setInputValue(FULL_NAME, information.getFullName());
        setOptionByVisibleText(DELIVERY_COUNTRY, information.getDeliveryCountry());
        setInputValue(ADDRESS_LINE_1, information.getAddressLine1());
        setInputValue(ADDRESS_LINE_2, information.getAddressLine2());
        setInputValue(CITY, information.getTownCity());
        setInputValue(COUNTY, information.getCountyState());
        setInputValue(POSTCODE, information.getPostcodeZIP());
        moveOutOfTheField(getChildElement(getPostcodeInput()));
        waitForPageReadyState(3);
    }

    private By getEmailInput() {
        return By.cssSelector("input[name='emailAddress']");
    }

    private By getEmailInputError() {
        return By.xpath(".//input[@name='emailAddress']/../div[@class='error-block']");
    }

    private By getFullNameInput() {
        return By.cssSelector("input[name='delivery-fullName']");
    }

    private By getFullNameInputError() {
        return By.xpath(".//input[@name='delivery-fullName']/../div[@class='error-block']");
    }

    private By getCountryInput() {
        return By.cssSelector("select[name='deliveryCountry']");
    }

    private By getAddressLine1Input() {
        return By.cssSelector("input[name='delivery-addressLine1']");
    }

    private By getAddressLine1InputError() {
        return By.xpath(".//input[@name='delivery-addressLine1']/../div[@class='error-block']");
    }

    private By getAddressLine2Input() {
        return By.cssSelector("input[name='delivery-addressLine2']");
    }

    private By getCityInput() {
        return By.cssSelector("input[name='delivery-city']");
    }

    private By getCityInputError() {
        return By.xpath(".//input[@name='delivery-city']/../div[@class='error-block']");
    }

    private By getCountyInput() {
        return By.cssSelector("input[name='delivery-county']");
    }

    private By getPostcodeInput() {
        return By.cssSelector("input[name='delivery-postCode']");
    }

    private By getPostcodeInputError() {
        return By.xpath(".//input[@name='delivery-postCode']/../div[@class='error-block']");
    }
}
