package desktop.fragments;

import abstractClasses.fragment.AbstractFragment;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BasketPopup extends AbstractFragment {

    @FindBy(css = "div.modal-content")
    private WebElement rootElement;

    public BasketPopup() {
        setRootElement(rootElement);
    }

    public void proceedToBasketCheckout() {
        clickChildElement(getToBasketCheckoutButton());
    }

    private By getToBasketCheckoutButton() {
        return By.cssSelector("a[href='/basket']");
    }
}
