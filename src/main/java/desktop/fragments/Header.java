package desktop.fragments;

import abstractClasses.fragment.AbstractFragment;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static utils.KeyboardUtils.submit;

public class Header extends AbstractFragment {
    @FindBy(css = "header.header")
    WebElement rootElement;

    public Header() {
        setRootElement(rootElement);
    }

    public void searchForProducts(String productName) {
        getChildElement(getSearchField()).sendKeys(productName);
        submit();
    }

    private By getSearchField() {
        return By.cssSelector("input[name='searchTerm']");
    }

}
