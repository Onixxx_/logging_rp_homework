package desktop.pages;

import abstractClasses.page.AbstractPage;
import desktop.fragments.forms.DeliveryAddressForm;
import desktop.fragments.forms.PaymentForm;
import desktop.fragments.order_summary.CheckoutOrderSummary;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static utils.JavaScriptExecutorActions.scrollToElement;

public class CheckoutPage extends AbstractPage {

    private final String URL = "https://www.bookdepository.com/payment";

    private final String URL_PATTERN = "[https://www.bookdepository.com/payment].+";

    @FindBy(css = "button[type='submit']")
    private WebElement buyNowButton;

    public CheckoutPage() {
        super();
        setPageUrl(URL);
        setPageUrlPattern(URL_PATTERN);
    }

    public void clickBuyNowButton() {
        scrollToElement(buyNowButton);
        buyNowButton.click();
    }

    public DeliveryAddressForm getDeliveryAddressForm() {
        return new DeliveryAddressForm();
    }

    public PaymentForm getPaymentForm() {
        return new PaymentForm();
    }

    public CheckoutOrderSummary getOrderSummary() {
        return new CheckoutOrderSummary();
    }
}
