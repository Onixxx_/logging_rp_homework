package stepDefs;

import desktop.fragments.forms.FormField;
import desktop.page_factory_manager.PageFactoryManager;
import driver.SingletonDriver;
import dto.Card;
import dto.DeliveryAddressInformation;
import io.cucumber.java.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static constants.Constants.IMPLICITLY_WAIT_TIMEOUT;
import static constants.DeliveryAddressFormConstants.*;
import static driver.SingletonDriver.getDriver;
import static driver.SingletonDriver.openPageByUrl;
import static org.assertj.core.api.Assertions.assertThat;
import static utils.WebDriverWaiter.waitForPageReadyState;
import static utils.WebDriverWaiter.waitForVisibilityOfElement;

public class CheckoutStepDefinitions {

    static {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_hh-mm-ss");
        System.setProperty("current.date.time", dateFormat.format(new Date()));
    }

    PageFactoryManager pageFactory;
    final static Logger logger = Logger.getLogger(CheckoutStepDefinitions.class);

    @Before
    public void onInit() {
        logger.info("TEST IS STARTED");
        if (pageFactory == null) {
            pageFactory = new PageFactoryManager();
        }
        SingletonDriver.createDriver();
        getDriver().manage().window().maximize();
        getDriver().manage().timeouts().implicitlyWait(IMPLICITLY_WAIT_TIMEOUT, TimeUnit.SECONDS);
    }

    @After
    public void tearDown(Scenario scenario) {

        if (scenario.isFailed()) {
            logger.error("TEST " + scenario.getName() + " FAILED!");
            try {
                byte[] screen = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BYTES);
                scenario.attach(screen, "image/png", scenario.getName());
                File screenshot = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.FILE);
                String path = "./screenshots/" + scenario.getName() + ".png";
                FileUtils.copyFile(screenshot, new File(path));

                logger.info("Screenshot is added");
            } catch (Exception e) {
                logger.warn("Some problem with saving screenshot");
            }
        }
        getDriver().close();
        logger.info("TEST IS ENDED");
    }

    @Given("I open {string} page and check header is {string} in {string}")
    public void openUrlVerifyHeader(String url, String header, String scenario) {
        logger.debug("Open url " + url);
        getDriver().get(url);
        assertThat(header)
                .withFailMessage("Header title is not as expected")
                .isEqualTo(getDriver().getTitle());
    }

    @Given("I am an anonymous customer with clear cookies")
    public void cleanCookies() {
        getDriver().manage().deleteAllCookies();
        logger.info("Cookies are clear");
    }

    @When("I open the {string} page")
    public void openPage(String page) {
        logger.debug("Open " + page + " page");
        openPageByUrl(pageFactory.getPageByName(page).getPageUrl());
        waitForPageReadyState(4);
    }

    @And("I search for {string}")
    public void search(String query) {
        pageFactory.getHomePage().getHeader().searchForProducts(query);
    }

    @And("I am redirected to the {string} page")
    public void isRedirectedToPage(String page) {
        assertThat(pageFactory.getPageByName(page).checkUrl())
                .withFailMessage("User is not redirected on page %s", page)
                .isTrue();
    }

    @And("Search results contain the following products")
    public void searchResultsContainTheFollowingProducts(List<String> productsNames) {
        assertThat(pageFactory.getSearchPage().getFoundedProductsNames().containsAll(productsNames))
                .withFailMessage("Results do not contain expected products")
                .isTrue();
    }

    @And("I apply the following search filters")
    public void applyTheFollowingSearchFilters(Map<String, String> filters) {
        pageFactory.getSearchPage().getSearchFiltersForm().selectFilters(filters);
    }

    @Then("Search results contain only the following products")
    public void searchResultsContainOnlyTheFollowingProducts(List<String> productsNames) {
        List<String> differences = pageFactory.getSearchPage().getFoundedProductsNames().stream()
                .filter(element -> !productsNames.contains(element))
                .collect(Collectors.toList());
        assertThat(differences.size())
                .withFailMessage("Results do not contain expected products")
                .isZero();
    }

    @When("I click 'Add to basket' button for product with name {string}")
    public void clickAddToBasketButtonForProductWithName(String productName) {
        pageFactory.getSearchPage().clickAddProductToBasket(productName);
    }

    @And("I select 'Basket\\Checkout' in basket pop-up")
    public void selectBasketCheckoutInBasketPopUp() {
        waitForVisibilityOfElement(4, pageFactory.getSearchPage().getBasketPopup().getRootElement());
        pageFactory.getSearchPage().getBasketPopup().proceedToBasketCheckout();
    }

    @And("Basket order summary is as following:")
    public void basketOrderSummaryIsAsFollowing(@Transpose Map<String, String> expectedTotals) {
        assertThat(pageFactory.getBasketPage().getOrderSummary().isOrderSummaryAsFollowing(expectedTotals))
                .withFailMessage("Basket order summary is not as expected")
                .isTrue();
    }

    @When("I click 'Checkout' button on 'Basket' page")
    public void clickCheckoutButtonOnBasketPage() {
        logger.debug("Open Checkout Payment page");
        pageFactory.getBasketPage().proceedToCheckoutPayment();
    }

    @When("I click 'Buy now' button")
    public void clickBuyNowButton() {
        pageFactory.getCheckoutPage().clickBuyNowButton();
    }

    @Then("the following validation error messages are displayed on 'Delivery Address' form:")
    public void validationErrorMessagesAreDisplayedOnDeliveryAddressForm(Map<String, String> expectedErrorMessages) {
        for (String inputField : expectedErrorMessages.keySet()) {
            String actualMsg = pageFactory.getCheckoutPage().getDeliveryAddressForm().getErrorMessageOfInput(inputField);
            assertThat(actualMsg)
                    .withFailMessage("Element %s has not error message %s", inputField,
                            expectedErrorMessages.get(inputField))
                    .isEqualTo(expectedErrorMessages.get(inputField));
        }
    }

    @And("the following validation error messages are displayed on 'Payment' form:")
    public void theFollowingValidationErrorMessagesAreDisplayedOnPaymentForm(String expectedMessages) {
        List<String> expectedMessagesList = new ArrayList<>(Arrays.asList(expectedMessages.split(", ")));
        assertThat(pageFactory.getCheckoutPage().getPaymentForm().getGlobalErrorMessages())
                .withFailMessage("Validation error messages are not displayed")
                .isEqualTo(expectedMessagesList);
    }

    @And("Checkout order summary is as following:")
    public void checkoutOrderSummaryIsAsFollowing(@Transpose Map<String, String> expectedTotals) {
        assertThat(pageFactory.getCheckoutPage().getOrderSummary().isOrderSummaryAsFollowing(expectedTotals))
                .withFailMessage("Checkout order summary is not as expected")
                .isTrue();
    }

    @And("I checkout as a new customer with email {string}")
    public void checkoutAsANewCustomerWithEmail(String email) {
        pageFactory.getCheckoutPage().getDeliveryAddressForm().provideEmail(email);
    }

    @When("I fill delivery address information manually:")
    public void fillDeliveryAddressInformationManually(DeliveryAddressInformation data) {
        pageFactory.getCheckoutPage().getDeliveryAddressForm().fillDeliveryAddressForm(data);
    }

    @Then("there is no validation error messages displayed on 'Delivery Address' form")
    public void thereIsNoValidationErrorMessagesDisplayedOnDeliveryAddressForm() {
        Map<String, FormField> errorMessages = pageFactory.getCheckoutPage().getDeliveryAddressForm().getFormFields();
        for (String key : errorMessages.keySet()) {
            if (errorMessages.get(key).getFieldErrorMessageLocator() != null) {
                assertThat(errorMessages.get(key).getFieldErrorMessageLocator().getText())
                        .withFailMessage("Error message is displayed for field %s", key)
                        .isEmpty();
            }
        }
    }

    @When("I enter my card details")
    public void enterCardDetails(Map<String, String> entry) {
        Card card = new Card(entry.get("cardNumber"), entry.get("Expiry Year"),
                entry.get("Expiry Month"), entry.get("Cvv"));
        pageFactory.getCheckoutPage().getPaymentForm().fillPaymentForm(card);
    }

    @DataTableType
    public DeliveryAddressInformation deliveryAddressInformationEntry(Map<String, String> entry) {
        return new DeliveryAddressInformation(entry.get(FULL_NAME), entry.get(DELIVERY_COUNTRY),
                entry.get(ADDRESS_LINE_1), entry.get(ADDRESS_LINE_2), entry.get(CITY), entry.get(COUNTY),
                entry.get(POSTCODE));
    }

}